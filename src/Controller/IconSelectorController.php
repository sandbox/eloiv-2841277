<?php
namespace Drupal\icons_visual_selector\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for dblog routes.
 */
class IconSelectorController extends ControllerBase {

  /**
   * A simple page to explain to the developer what to do.
   */
  public function description() {
    return array(
      '#title' => 'Icon Selector',
      '#markup' => 'Select an icon',
    );
  }

}
