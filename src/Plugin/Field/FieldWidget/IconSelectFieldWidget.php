<?php

namespace Drupal\icons_visual_selector\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Plugin implementation of the 'icon_select_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "icon_select_field_widget",
 *   label = @Translation("Icon select"),
 *   field_types = {
 *     "list_string"
 *   }
 * )
 */
class IconSelectFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $value_list = $items->getSettings()['allowed_values'];
    $default_value = $items->getValue();
    $default_value = $default_value[0]['value'];
    $element['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Icon'),
      '#placeholder' => $this->t('placeholder'),
      '#options' => $items->getSettings()['allowed_values'],
      '#suffix'=> '<div class="icon-badge"><span id="icon_preview" class="icon -'.$default_value.'"></span></div>',
      '#default_value' => $default_value,
      //add css and js libraries from icons_visual_selector.libraries.yml
      '#attached' => array(
        'library' => array(
          'icons_visual_selector/icon-selector'
        ),
      ),
    ];
    return $element;
  }
}
