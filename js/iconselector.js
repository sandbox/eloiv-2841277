(function($, Drupal, drupalSettings) {
    Drupal.behaviors.iconselector = {
        attach: function (context, settings) {
            $(".field--name-field-icon select").once().click(function(){
                var iconClass = $(this).val();
                $(this).parent().next().children().removeClass();
                $(this).parent().next().children().addClass('icon -' + iconClass);

            });
        }
    };

})(jQuery, Drupal, drupalSettings);
